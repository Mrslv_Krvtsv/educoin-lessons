//Урок 6
//Пользователь получает модальное окно с просьбой ввести имя(prompt)
	//Если пользователь ввел адекватное имя которое соответствует 
	//следующим условиям: это текст, имеет более 3 символов и 
	//не имеет в себе цифр, то добавляем это имя в массив имен
	//Если пользователь ввел значение "0", то завершить программу 
	//и в цикле вывести все имена в консоль(console.log("text")).
	//Пример вывода имен: "Имя пользователя 1 - Евгений"
//Программа выполняется до тех пор пока пользователь не введет "0".
//Иначе ему постоянно выскакивает окошко с просьбой ввести имя.

//Урок 7
//Добавить новое имя в массив. - было, есть
//Удалить значение из массива по имени. - ура, работает
//Изменить имя по найденному имени в массиве. - ураааа, работает
//Поиск имени в массиве, если имя найдено вернуть индекс элемента и
//имя, иначе вернуть "имя не найдено"		-есть

"use strict";

var arrNames = [];
var GlobalElement;
var GlobalIndex;

console.log("Итак, что ты можешь сделать: ");
console.log("InputNames(); - ввести элементы массива (4 символа и больше без цифр),");
console.log("OutputNames(); - вывести элементы массива,");
console.log("FindName(); - найти имя в массиве,");
console.log("DeleteName(); - удалить имя из массива,");
console.log("ChangeName(); - изменить имя в массиве.");
console.log(" ");
console.log("Приступаем =)");

function InputNames() {
	do {
		var Name = prompt("Enter name: ");
		var rez = Name;
		if (rez.replace (/\D/g, '').length) {	//D - любой нецифровой символ
		continue;
		}
		if(Name.length <= 3 ) {
			continue;
		}
		arrNames.push(Name);
	} while (Name != "0");
}

function OutputNames() {
	for (var i=0; i<arrNames.length; i++) {
		console.log("Имя пользователя "+ (i + 1) +" - " + arrNames[i]);
	}
}

function Find(array) {
	var value = prompt("Введи искомое имя:");
	for (var i = 0; i < array.length; i++) {
		if (array[i] === value) {
		GlobalElement = array[i];
		GlobalIndex = i;
		}
	}
	i=0;
	return -1; 		
}

function FindName(){
	var index = Find(arrNames);
	if (GlobalElement == undefined) {
		console.log("Имя не найдено!");
	} else {
		console.log("Номер в массиве: " + (GlobalIndex+1) + " Имя: " + GlobalElement);
	}
	GlobalElement=undefined; 
}


function DeleteName() {
	var index = Find(arrNames);
	if (index != -1 || GlobalElement == undefined) {
		console.log("Имя не найдено!");		
	} else {	
		console.log("Номер в массиве: " + (GlobalIndex+1) + " Имя: " + GlobalElement);
		arrNames.splice(GlobalIndex,1);	
		console.log("Удалено!");
	}
	GlobalElement=undefined;
}

function ChangeName() {
	var index = Find(arrNames);
	if (index != -1 || GlobalElement == undefined){
		console.log("Имя не найдено!");
	} else {
		console.log("Номер в массиве: " + (GlobalIndex+1) + " Имя: " + GlobalElement);
		var value = prompt("Введи новое имя:");
		if(value.length <= 3 ) {
			console.log("Слишком короткое имя!");
			return false;
		}		
		if (value.replace (/\D/g, '').length) {	//D - любой нецифровой символ
			console.log("Введенное содержит цифры!");
			return false;
		}
		arrNames.splice(GlobalIndex,1,value);	
		console.log(" Изменено! ");
	}
	GlobalElement=undefined;
}