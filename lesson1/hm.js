"use strict";

var names = [];

function add (name) {
	alert("test");
	var i;

	if ( name.length <= 3 || !isNaN( name ) ) return "Error name";

	for (i = 0; i < name.length; i++) {
		if( !isNaN( name[i] ) ) return "Error name";
	}

	names.push(name);
}

function edit (name, newName) {
	var res = search(name),
	tmp;

	if( res != -1 ) {
		tmp = names[res];
		names[res] = newName;


		return console.log("Пользователь с именем " + tmp + " был переименован на - " + newName);

	} 

	return "Пользователь не найден"
}

function remove (name) {
	var res = search(name);

	if( res != -1 ) {
		console.log("Пользователь удален - " + (res + 1) + " - " + names[res]);

		return names.splice(res, 1);
	} 

	return "Пользователь не найден"

}

function inArray (name) {
	var i;

	for (i = 0; i < names.length; i++ ) {
		if(names[i] === name) {
			return i;
		}
	}

	return -1;
}

function search (name) {
	var res = search(name);

	if( res != -1 ) {
		console.log("Пользователь найден - " + (res + 1) + " - " + names[res]);

		return 0;
	} 

	return "Пользователь не найден"
}

function all () {

	var i;

	for (i = 0; i < names.length; i++) {
		console.log("Имя пользователя " + (i + 1) + " - " + names[i]);
	}

}

