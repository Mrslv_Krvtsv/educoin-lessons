"use strict";

// глобальные переменные
// contacts - все контакты 
// displayedContacts - контакты которые отображаются на странице
var contacts, displayedContacts;


// Массив всех контактов
displayedContacts = contacts = [
	{
        id: 1,
        name: 'Darth Vader',
        phoneNumber: '+250966666666',
        image: 'http://cs7.pikabu.ru/images/big_size_comm_an/2014-03_7/13962622876915.gif'
    }, {
        id: 2,
        name: 'Princess Leia',
        phoneNumber: '+250966344466',
        image: 'http://images6.fanpop.com/image/photos/33100000/CARRIE-FISHER-anakin-vader-and-princess-leia-33186069-190-149.gif'
    }, {
        id: 3,
        name: 'Luke Skywalker',
        phoneNumber: '+250976654433',
        image: 'https://media.tenor.com/images/96684744d07589f309d2c5c8b774604d/tenor.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        phoneNumber: '+250456784935',
        image: 'https://media.giphy.com/media/RUUdVZqwpfTRS/giphy.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        phoneNumber: '+250456784935',
        image: 'https://media.giphy.com/media/RUUdVZqwpfTRS/giphy.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        phoneNumber: '+250456784935',
        image: 'https://media.giphy.com/media/RUUdVZqwpfTRS/giphy.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        phoneNumber: '+250456784935',
        image: 'https://media.giphy.com/media/RUUdVZqwpfTRS/giphy.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        phoneNumber: '+250456784935',
        image: 'https://media.giphy.com/media/RUUdVZqwpfTRS/giphy.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        phoneNumber: '+250456784935',
        image: 'https://media.giphy.com/media/RUUdVZqwpfTRS/giphy.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        phoneNumber: '+250456784935',
        image: 'https://media.giphy.com/media/RUUdVZqwpfTRS/giphy.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        phoneNumber: '+250456784935',
        image: 'https://media.giphy.com/media/RUUdVZqwpfTRS/giphy.gif'
    }
];


/**
* 
* @var string searchValue
* @todo перебирает массив всех контактов
* @todo если есть совпадения в имени, то заносит в массив contactsList
* @return array contactsList
*/
function contactSearch(searchValue) {
	var i,
		contactsList = [];

	// преобразовать то что вошло в нижний регистр
	searchValue = searchValue.toLowerCase();

	for (i = 0; i < contacts.length; i++) {

		// преобразовываем имя контакта в нижний регистр
		// и ищем совпадения в строке
        if( contacts[i].name.toLowerCase().indexOf(searchValue) !== -1 ) {

        	contactsList.push(contacts[i]);

        }

	}

	return contactsList;	
}

/**
* @var array contacts
* @todo перебирает массив контактов для отображения
* @todo формирует html для отображения
* @return string html
*/
function contactsShow(contacts) {
	var i, html = "";
	
	for (i = 0; i < contacts.length; i++ ) {
		html += '<li class="contact">' +
					'<img class="contact-image" src="' + contacts[i].image + '" width="60px" height="60px" />'	+
					'<div class="contact-info">' +
						'<div class="contact-name">' +
			            	'<span>' + contacts[i].name + '</span>' +
		          		'</div>' +
		          		'<div class="contact-number">' +
			            	'<span>' + contacts[i].phoneNumber + '</span>' + 
			          	'</div>' +
		          	'</div>' +
	          	'</li>';
	}

	return html;
}

/**
* навешиваем на событие onload функцию
*/
window.onload = function() {
	var textSearch, contactsList;

	textSearch = document.getElementById("text-search");
	contactsList = document.getElementById("contacts-list");

	contactsList.innerHTML = contactsShow(displayedContacts);

	// Навешиваем обработчик onkeyup на поле для ввода
	textSearch.onkeyup = function(e) {

		contactsList.innerHTML = contactsShow(contactSearch(e.target.value));

	}

};